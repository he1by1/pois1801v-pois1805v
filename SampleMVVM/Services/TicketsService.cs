﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using SampleMVVM.Infrastructure;
using SampleMVVM.Models;

namespace SampleMVVM.Services
{
    public class TicketsService
    {
        public TicketsService() { 
       
        }
        /// <summary>
        /// Метод который возвращает все билеты
        /// </summary>
        /// <returns>Лист биллетов</returns>
        public List<Ticket> GetAllTickets()
        {
            var dataBaseHelper = DatabaseHelper.Instance;
            var tickets = dataBaseHelper.GetTicketsTable();
            return tickets;
        }

        /// <summary>
        /// Метод который возвращает все билеты с типом "Free"
        /// </summary>
        /// <returns>Лист биллетов с типом Free</returns>
        public List<Ticket> GetFreeTickets()
        {
            var dataBaseHelper = DatabaseHelper.Instance;
            var tickets = dataBaseHelper.GetTicketsTable();
            var freeTickets = tickets
                              .Where(ticket => ticket.TicketsType.Equals(TicketsType.Free))
                              .ToList();
            return freeTickets;
        }
    }
}
