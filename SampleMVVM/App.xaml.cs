﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Windows;
using SampleMVVM.Models;
using SampleMVVM.ViewModels;
using SampleMVVM.Views;
using SampleMVVM.Services;
using SampleMVVM.Infrastructure.Builders.ShowBuilders;
using SampleMVVM.Infrastructure.Director;
using SampleMVVM.Infrastructure.Prototype;

namespace SampleMVVM
{
    /// <summary>
    /// Interaction logic for App.xaml
    /// </summary>
    public partial class App : Application
    {
        private void OnStartup(object sender, StartupEventArgs e)
        {
            var shows = new List<Show>();
            var ticketsService = new TicketsService();
            var vipShowBuilder = new VipShowBuilder();
            var director = new Director();
            director.Construct(vipShowBuilder);
            var newShow = director.GetShow();
            newShow.GroupName = "Группа1";
            shows.Add(newShow);
            var vipShow = new VipShowPrototype();
            vipShow.GroupName = "Название вип группы";
            vipShow.DayName = "Вторник";
            vipShow.Tickets = new List<Ticket> { 
                new Ticket { 
                    Price = 0, 
                    TicketsType = TicketsType.Free
                } 
            };
            shows.Add(vipShow.Clone());
            MainView view = new MainView(); // создали View
            MainViewModel viewModel = new ViewModels.MainViewModel(shows); // Создали ViewModel
            view.DataContext = viewModel; // положили ViewModel во View в качестве DataContext
            view.Show();
        }
    }
}
