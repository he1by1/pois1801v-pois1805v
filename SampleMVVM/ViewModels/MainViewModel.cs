﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Input;
using SampleMVVM.Commands;
using System.Collections.ObjectModel;
using SampleMVVM.Models;

namespace SampleMVVM.ViewModels
{
    class MainViewModel : ViewModelBase
    {
        public ObservableCollection<ShowViewModel> ShowsList { get; set; } 

        #region Constructor

        public MainViewModel(List<Show> shows)
        {
            ShowsList = new ObservableCollection<ShowViewModel>(shows.Select(show => new ShowViewModel(show)));
        }

        #endregion
    }
}
