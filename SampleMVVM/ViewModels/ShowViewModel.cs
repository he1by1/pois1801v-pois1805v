﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using SampleMVVM.Models;
using System.Windows.Input;

namespace SampleMVVM.ViewModels
{
    public class ShowViewModel : ViewModelBase
    {
        public Show Show { get; set; }
        public ShowViewModel(Show show)
        {
            this.Show = show;
        }

        public string GroupName
        {
            get
            {
                return Show.GroupName;
            }
            set
            {
                Show.GroupName = value;
                OnPropertyChanged("GroupName");
            }
        }

        public string DayName
        {
            get
            {
                return Show.DayName;
            }
            set
            {
                Show.DayName = value;
                OnPropertyChanged("DayName");
            }
        }

        public int TicketsCount
        {
            get
            {
                return Show.Tickets.Count();
            }
        }

        public Ticket GetTicketCommand
        {
            get
            {
                if (Show.Tickets.Any())
                {
                    var ticket = Show.Tickets.Last(x => x.TicketsType == TicketsType.Paid);
                    return ticket;
                } 
                return default(Ticket);
            }
        }
    }
}

