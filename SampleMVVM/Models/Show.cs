﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace SampleMVVM.Models
{
    public abstract class Show
    {
        public abstract Show Clone(); 
        public Show() { }
        public Show(string GroupName, string DayName, List<Ticket> Ticket) {
            this.GroupName = GroupName;
            this.DayName = DayName;
            this.Tickets = Ticket;
        }

        //Имя группы
        public string GroupName { get; set; }
        public string DayName { get; set; }
        public List<Ticket> Tickets { get; set; }

    }
    
    public class Ticket {
        //Типы билетов
        public TicketsType TicketsType { get; set; }

        //Цена билета
        public float Price { get; set; }
    }

    public enum TicketsType
    {
        Free,
        Paid,
        Vip
    }
}
