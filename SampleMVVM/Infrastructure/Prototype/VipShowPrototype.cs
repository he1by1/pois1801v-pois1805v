﻿using SampleMVVM.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace SampleMVVM.Infrastructure.Prototype
{
    public class VipShowPrototype: Show
    {
        public override Show Clone()
        {
            //Shallow copy 
            return (Show)this.MemberwiseClone();
        }
    }
}
