﻿using SampleMVVM.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace SampleMVVM.Infrastructure.Interfaces
{
    public interface IShowBuilder
    {
        void BuildTickets();
        void BuildDate();
        Show GetShow();
    }
}
