﻿using SampleMVVM.Infrastructure.Interfaces;
using SampleMVVM.Infrastructure.Prototype;
using SampleMVVM.Models;
using SampleMVVM.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace SampleMVVM.Infrastructure.Builders.ShowBuilders
{
    public class VipShowBuilder : IShowBuilder
    {
        private VipShowPrototype _show = new VipShowPrototype();
        public void BuildTickets()
        {
            var ticketsService = new TicketsService();
            _show.Tickets = ticketsService.GetAllTickets();
        }

        public void BuildDate()
        {
            _show.DayName = "Вторник";
        }

        public Show GetShow()
        {
           return _show;
        }
    }
}
