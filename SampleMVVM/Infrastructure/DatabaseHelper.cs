﻿using SampleMVVM.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
/*
 * public sealed class Singleton {
 * static readonly Lazy<Singletone> lazy = new Lazy<Singletone>(() => new Singleton());
 * private Singleton(){}
 * public static Signleton_Instance => lazy.Value; 
 * }
  */
namespace SampleMVVM.Infrastructure
{
    /// <summary>
    /// sealed = запрещённый для наследования
    /// </summary>
    public sealed class DatabaseHelper
    {
        /// <summary>
        /// volatile - запрещает кэширование объекта
        /// </summary>
        private static volatile DatabaseHelper _instance;
        private static object _locker = new Object();
        private List<Ticket> _ticketsTable;
        private DatabaseHelper() {
            _ticketsTable = new List<Ticket>{
                new Ticket { 
                Price = 1,
                TicketsType = TicketsType.Paid
                },
                new Ticket { 
                TicketsType = TicketsType.Free
                }
            };
        }
        public static DatabaseHelper Instance {
            get
            {
                if (_instance == null)
                {
                    ///lock - блокирует объект внутри скобок для изменения из других потоков
                    ///запрещает выполнять код внутри {} , пока 1 из потоков его выполняет 
                    lock (_locker)
                    {
                        if (_instance == null)
                        {
                            _instance = new DatabaseHelper();
                        }
                    }
                }
                return _instance;
            }
        }
        public List<Ticket> GetTicketsTable() {
            return _ticketsTable;
        }
    }
}
