﻿using SampleMVVM.Infrastructure.Interfaces;
using SampleMVVM.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace SampleMVVM.Infrastructure.Director
{
    public class Director
    {
        private IShowBuilder IShowBuilder;
        public void Construct(IShowBuilder IShowBuilder)
        {
            this.IShowBuilder = IShowBuilder;
            IShowBuilder.BuildDate();
            IShowBuilder.BuildTickets();
        }

        public Show GetShow() {
            return this.IShowBuilder.GetShow();
        }
    }
}
